INTRODUCTION
------------

The Content Sneak Peek module provides a easy way for site developers to display
dynamic or static content using a "sneak peek" display, which can be rendered
on any position of the page that he wants.

This kind of display can be used to improve the user usability by enabling them
to see a summary or all fields of a desired content without having to exit the
current page and also without the need of clustering the page with more
information.

The main idea of this module is not to provide a static plug-and-play
functionality, but to provide a sturdy way to render anything depending on your
own requirements, from a simple content display to entire forms.


MAINTAINERS
-----------

Current maintainers:
  * Felipe Caminada - https://www.drupal.org/u/caminadaf

This project has been sponsored by:
  * CI&T - CI&T has more experience than anyone else in leading Drupal
    implementations for multi-brand enterprises. When you need to provide better
    more timely, and relevant web experiences for your customers, CI&T takes
    you where you need to be. (http://drupal.org/node/1530378)


REQUIREMENTS
------------

This module has no dependencies.


INSTALLATION
------------

No special installation needed.


CONFIGURATION
------------

After the module is enabled, you can start rendering a default sneak peek right
away by simply rendering a content with the 'content_sneak_peek_wrapper' theme:

  print theme('content_sneak_peek_wrapper', array(
    'settings' => array(
      'selector' => '.open-sneak-peek',
      'content' => 'Test Sneak Peek',
    ),
  ));

An alternative way is by using a renderable array:

  $element = array(
    '#theme' => 'content_sneak_peek_wrapper',
    '#settings' => array(
      'selector' => '.open-sneak-peek',
      'content' => 'Test Sneak Peek',
    ),
  );

This will make any element which contains the "open-sneak-peek" class to open a
sneak peek content on its click event rendering the "Test Sneak Peek" string.

If you don't set 'static' as TRUE on the theme call, a default AJAX
function will be called which will simply get the ID of the element that was
clicked and render it inside the Sneak Peek area.

A custom AJAX call can be implemented using defaultAjaxCall as base.

* List of variables that can be used:
  content:
    A static content to be rendered.
  selector:
    A selector to the element that will trigger the sneak peek animation.
  wrapper_id:
    An optional id for this sneak peek.
  position:
    The position, relative to the page, for the sneak to be rendered.
  speed:
    Sneak Peek animation speed in milliseconds.
  static:
    If the content to be displayed is static. If set to FALSE, then the
    'js_callback' will be used to render content with AJAX.
  js_callback:
    A javascript function that will be called to get the parameters that will
    be sent to the ajax callback.
    If not provided, the sneak peek content will not be replaced before the
    animation start.
    This function must be callable from content_sneak_peek.js and return a
    json with the following format:
    {
      'ajax_callback': 'you_php_callback',
      'parameters': {
        param1: value1,
        param2: value2
      }
    }.

The defaults for each variable can be seen at content_sneak_peek_variables().
