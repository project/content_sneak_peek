<?php

/**
 * @file
 * Content Sneak Peek Template.
 */
?>
<div id="<?php print $wrapper_id; ?>" <?php !empty($wrapper_classes) ? print 'class="' . $wrapper_classes . '"' : ''; ?>>
  <div class="close-content-sneak-peek">
    <a href="#close">
     <span><?php print t('Close'); ?></span>
    </a>
  </div>
  <div id="content-sneak-peek">
    <?php print $content; ?>
  </div>
</div>
