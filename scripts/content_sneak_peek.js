/**
 * @file
 * Handle the content sneak peek animation.
 */

(function ($) {
  'use strict';

  Drupal.ContentSneakPeek = {
    // A JS callback that calls a default AJAX function.
    defaultAjaxCall: function (element) {
      return {
        ajax_callback: 'content_sneak_peek_default_ajax_callback',
        parameters: $(element).prop('id')
      };
    },

    // Gets the sneak peek content.
    setContent: function (wrapper, content, ajax_options) {
      wrapper.find('#content-sneak-peek').html(content);
      if (typeof ajax_options !== 'undefined' && ajax_options.hasOwnProperty('completeCallback')) {
        ajax_options.completeCallback();
      }
    },

    // Loads the content and resets the behaviors.
    loadContent: function (wrapper, settings, ajax_options, url) {
      if (typeof url !== 'undefined') {
        $.get(url).done(function (data) {
          Drupal.settings.contentSneakPeek.content = data.content;
          wrapper.removeClass('loading');
          Drupal.ContentSneakPeek.setContent(wrapper, settings.content, ajax_options);
          Drupal.attachBehaviors(wrapper);
        });
      }
    },

    // Animates the Sneak peek and loads the content.
    animate: function (wrapper, options, settings, ajax_options, url) {
      if (typeof ajax_options !== 'undefined' && ajax_options.hasOwnProperty('startCallback')) {
        ajax_options.startCallback();
      }
      // Animates if the sneak peek is not yet visible.
      if (!wrapper.is(':visible')) {
        wrapper.not(':animated').animate(
          options,
          settings.speed,
          'swing',
          Drupal.ContentSneakPeek.loadContent(wrapper, settings, ajax_options, url)
        );
        return false;
      }

      Drupal.ContentSneakPeek.loadContent(wrapper, settings, ajax_options, url);
      return false;
    },

    // Finds the desired function by its name.
    findFunction: function (function_name) {
      if (!function_name.length) {
        return null;
      }

      var target = window;
      var func = function_name.split('.');
      while (func.length) {
        var ns = func.shift();
        if (typeof target[ns] === 'undefined') {
          return null;
        }
        target = target[ns];
      }

      if (typeof target === 'function') {
        return target;
      }

      return null;
    }
  };

  // Sneak Peek behavior.
  Drupal.behaviors.contentSneakPeek = {
    attach: function (context) {
      var settings = Drupal.settings.contentSneakPeek;
      var $wrapper = '.content-sneak-peek-wrapper';
      // Gets the sneak peek wrapper.
      if (settings.wrapper_id !== '' && 'undefined' !== typeof settings.wrapper_id) {
        $wrapper = '#' + settings.wrapper_id;
      }
      $wrapper = $($wrapper);
      $wrapper = $wrapper.detach();
      $wrapper.appendTo($('body'));
      // Binds the sneak peek toggle.
      if (settings.selector !== '') {
        var options = {};

        switch (settings.position) {
          case 'right':
          case 'left':
            options.width = 'toggle';
            break;

          case 'top':
          case 'bottom':
            options.height = 'toggle';
            break;
        }
        // Binds the click event only once per context.
        $(settings.selector, context).once('sneakpeekopen').on('click', function (event) {
          event.preventDefault();
          // Avoid triggering another ajax call if one already running.
          if ($wrapper.hasClass('loading')) {
            return;
          }

          // Overwrite the default sneak peek content if AJAX is to be used.
          if (settings.js_callback !== '' && !settings.static) {
            // Call the javascript callback that will return the parameters
            // used on ajax.
            var callback = Drupal.ContentSneakPeek.findFunction(settings.js_callback);
            if (null === callback) {
              // Invalid callback.
              return;
            }

            var ajax_options = callback(this);
            var url = '/ajax/content-sneak-peek/' +
               ajax_options.ajax_callback +
              '/' + JSON.stringify(ajax_options.parameters);

            $wrapper.addClass('loading');
            Drupal.ContentSneakPeek.animate($wrapper, options, settings, ajax_options, url);
            return;
          }
          Drupal.ContentSneakPeek.animate($wrapper, options, settings);
          return;
        });
      }
    }
  };

  // Close sneak peek.
  Drupal.behaviors.closeContentSneakPeek = {
    attach: function (context) {
      // Binds the click event only once per context.
      $('.close-content-sneak-peek a').once('sneakpeekclose').on('click', function (event) {
        $('.content-sneak-peek-wrapper', context).hide();
        event.preventDefault();
      });
    }
  };
})(jQuery);
